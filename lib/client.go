// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the BSD 3-Clause License.
// See accompanying files LICENSE.

package nat

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

type Client struct {
	conn net.Conn
	rd   *bufio.Reader
}

func NewClient(conn net.Conn) *Client {
	return &Client{
		conn: conn,
		rd:   bufio.NewReader(conn),
	}
}

func (c *Client) parseResponse() ([]string, error) {
	c.conn.SetReadDeadline(time.Now().Add(time.Minute))

	line, err := c.rd.ReadString('\n')
	if err != nil {
		return nil, err
	}
	line = strings.TrimSpace(line)
	params := strings.Split(line, " ")
	if len(params) == 0 {
		return nil, fmt.Errorf("invalid response format")
	}
	if params[0] == "OK" {
		return params[1:], nil
	} else {
		fmt.Println(params[0])
	}
	return nil, nil
}

func (c *Client) queryData(key string) ([]string, error) {
	var line = fmt.Sprintf("QUERY %s\n", key)
	_, err := c.conn.Write([]byte(line))
	if err != nil {
		return nil, err
	}

	params, err := c.parseResponse()
	fmt.Printf("result: %s\n", params)
	return params, nil
}

func (c *Client) QueryAll() ([]*Record, error) {
	params, err := c.queryData("")
	if err != nil {
		return nil, err
	}
	var list []*Record
	if err = json.Unmarshal([]byte(params[0]), &list); err != nil {
		return nil, err
	}
	return list, nil
}

func (c *Client) Query(key string) (*Record, error) {
	params, err := c.queryData(key)
	if err != nil {
		return nil, err
	}
	var r Record
	if err = json.Unmarshal([]byte(params[0]), &r); err != nil {
		return nil, err
	}
	return &r, nil
}

func (c *Client) Register(key string) error {
	var line = fmt.Sprintf("REGISTER %s\n", key)
	_, err := c.conn.Write([]byte(line))
	if err != nil {
		return err
	}
	body, err := c.parseResponse()
	if err != nil {
		return err
	}
	log.Printf("register status: %s\n", body)
	return nil
}
