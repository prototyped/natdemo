// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the BSD 3-Clause License.
// See accompanying files LICENSE.

package nat

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strings"
	"sync"
	"time"
)

type Proxy struct {
	done    chan struct{}
	mtx     sync.Mutex
	records map[string]*Record
	conn    *net.UDPConn
	buffer  []byte
	expire  int
}

func NewProxy(conn *net.UDPConn, expire int) *Proxy {
	return &Proxy{
		conn:    conn,
		expire:  expire,
		done:    make(chan struct{}),
		buffer:  make([]byte, 1024),
		records: make(map[string]*Record),
	}
}

func (s *Proxy) Run() {
	go s.CleanExpired()
	for {
		nbytes, addr, err := s.conn.ReadFrom(s.buffer)
		if err != nil {
			log.Printf("Accept: %v\n", err)
			break
		}
		var buf = bytes.NewBuffer(s.buffer[:nbytes])
		s.handleConn(addr, buf)
	}
	close(s.done)
}

func (s *Proxy) handleConn(addr net.Addr, rd *bytes.Buffer) error {
	for {
		line, err := rd.ReadString('\n')
		if err != nil {
			return err
		}
		line = strings.TrimSpace(line)
		params := strings.Split(line, " ")
		response, err := s.ExecCommand(addr, params)
		var buf = PackResponse(err, response)
		_, err = s.conn.WriteTo(buf.Bytes(), addr)
		if err != nil {
			log.Printf("SendResponse: %v\n", err)
			return err
		}
	}
	return nil
}

func (s *Proxy) CleanExpired() {
	var ticker = time.NewTicker(time.Second * time.Duration(s.expire))
	defer ticker.Stop()
	for {
		select {
		case now := <-ticker.C:
			s.mtx.Lock()
			for k, rec := range s.records {
				if now.Sub(rec.timestamp) >= time.Second*30 {
					delete(s.records, k)
				}
			}
			s.mtx.Unlock()

		case <-s.done:
			return
		}
	}
}

func (s *Proxy) ExecCommand(remoteAddr net.Addr, params []string) ([]byte, error) {
	if len(params) == 0 {
		return nil, fmt.Errorf("command not recognized")
	}
	var command = strings.ToUpper(params[0])
	params = params[1:]
	switch command {
	case "DISCOVER":
		return s.Discovery(remoteAddr, params)
	case "REGISTER":
		return s.Register(remoteAddr, params)
	case "QUERY":
		return s.Query(remoteAddr, params)
	case "READY":
		return s.Ready(remoteAddr, params)
	default:
		return nil, fmt.Errorf("unsupported command %f", command)
	}
}

// register my IP:port as records
func (s *Proxy) Register(remoteAddr net.Addr, params []string) ([]byte, error) {
	var key = ""
	if len(params[0]) > 0 {
		key = params[0]
	}
	if key == "" {
		return nil, fmt.Errorf("invalid register param")
	}

	host, port, err := net.SplitHostPort(remoteAddr.String())
	if err != nil {
		return nil, err
	}
	var record = &Record{
		IP:        host,
		Port:      port,
		Key:       key,
		addr:      remoteAddr,
		timestamp: time.Now(),
	}
	log.Printf("register key %s of %s:%s\n", key, host, port)
	s.mtx.Lock()
	s.records[key] = record
	s.mtx.Unlock()
	return nil, nil
}

// Query records with json format
func (s *Proxy) Query(remoteAddr net.Addr, params []string) ([]byte, error) {
	var key = ""
	if len(params) > 0 {
		key = params[0]
	}

	log.Printf("query key [%s]\n", key)
	if key == "" {
		s.mtx.Lock()
		defer s.mtx.Unlock()
		data, err := json.Marshal(s.records)
		if err != nil {
			return nil, err
		}
		return data, nil
	} else {
		s.mtx.Lock()
		defer s.mtx.Unlock()
		var v = s.records[key]
		if v != nil {
			data, err := json.Marshal(v)
			if err != nil {
				return nil, err
			}
			return data, nil
		}
		return nil, fmt.Errorf("key not found")
	}
}

// tell peer ready
func (s *Proxy) Ready(remoteAddr net.Addr, params []string) ([]byte, error) {
	if len(params) == 0 {
		return nil, fmt.Errorf("invalid ready param")
	}
	var key = params[0]
	if key == "" {
		return nil, fmt.Errorf("invalid ready param")
	}
	s.mtx.Lock()
	var record = s.records[key]
	s.mtx.Unlock()
	if record == nil {
		return nil, fmt.Errorf("key %s not found", key)
	}

	fmt.Printf("%s ready for %v\n", remoteAddr, record.Key)

	data := fmt.Sprintf("READY %s ", remoteAddr.String())
	buf := PackResponse(nil, []byte(data))
	_, err := s.conn.WriteTo(buf.Bytes(), record.addr)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

// NAT type discovery
func (s *Proxy) Discovery(remoteAddr net.Addr, params []string) ([]byte, error) {
	fmt.Printf("try discover NAT type of %v\n", remoteAddr)
	for i := 0; i < 3; i++ {
		//fmt.Println("Trying Address Restricted response")
		newConn, err := net.Dial("udp", remoteAddr.String())
		if err != nil {
			return nil, err
		}
		defer newConn.Close()
		newConn.Write([]byte("OK Address-Restricted-Cone-NAT\n"))

		//fmt.Println("Trying Port Restricted response")
		s.conn.WriteTo([]byte("OK Port-Restricted-Cone-NAT\n"), remoteAddr)
	}
	return nil, nil
}
