// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the BSD 3-Clause License.
// See accompanying files LICENSE.

package nat

import (
	"bytes"
	"net"
	"time"
)

type Record struct {
	IP        string    `json:"ip"`
	Port      string    `json:"port"`
	Key       string    `json:"key"`
	addr      net.Addr  ``
	timestamp time.Time ``
}

func PackResponse(err error, data []byte) *bytes.Buffer {
	var buf bytes.Buffer
	if err == nil {
		buf.WriteString("OK")

	} else {
		buf.WriteString("ERR")
	}
	buf.WriteByte(' ')
	buf.Write(data)
	buf.WriteByte('\n')
	return &buf
}
