package main

import (
	"flag"
	"log"
	"net"

	nat "github.com/ichenq/nat/lib"
)

var addr string
var expireSec int

func init() {
	flag.StringVar(&addr, "addr", ":8082", "host address to listen")
	flag.IntVar(&expireSec, "expire", 60, "expiration timeout seconds")
}

func main() {
	laddr, err := net.ResolveUDPAddr("udp", addr)
	if err != nil {
		log.Printf("ResolveUDPAddr: %v\n", err)
		return
	}
	conn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		log.Printf("Listen at: %v\n", err)
		return
	}
	defer conn.Close()

	log.Printf("Server start at %v\n", addr)
	proxy := nat.NewProxy(conn, expireSec)
	proxy.Run()
}
