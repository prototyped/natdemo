#!/usr/bin/python3
#! -*- coding:utf-8 -*-
#
# Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
# Distributed under the terms and conditions of the BSD 3-Clause License.
# See accompanying files LICENSE.

import socket
import struct
import json
import time
import argparse



def request(sock, msg):
    sock.send(msg.encode('utf-8'))
    pair = sock.recvfrom(1024)
    #print('recv from', pair[1])
    resp = pair[0].decode('utf-8')
    params = resp.split(' ')
    if params[0] == 'OK':
        return params[1:]
    print(params[0])


def sendto(sock, line):
    params = line.split(' ')
    host = params[1]
    port = int(params[2])
    msg = params[3] + '\n'
    data = msg.encode('utf-8')
    sock.sendto(data, (host, port))
    print('sent', host, port, msg)

def internal_command(sock, line):
    commands = {
        'sendto': sendto
    }
    for k in commands:
        if line.startswith(k):
            commands[k](sock, line)
    return False


def echo_comand(args):
    s = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, proto=socket.IPPROTO_UDP)
    s.connect((args.host, args.port))

    print('Enter command each line:')
    while True:
        line = input('')
        if line.startswith('sendto'):
            internal_command(s, line)
        else:
            resp = request(s, line + '\n')
            print(resp)
    resp = request(s, 'REGISTER %s\n' % args.key)

    s.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, default='3bodygame.com')
    parser.add_argument("--port", type=int, default=8082)
    args = parser.parse_args()

    echo_comand(args)


if __name__ == '__main__':
    main()
