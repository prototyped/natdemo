#!/usr/bin/python3
#! -*- coding:utf-8 -*-
#
# Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
# Distributed under the terms and conditions of the BSD 3-Clause License.
# See accompanying files LICENSE.

import socket
import json
import argparse

def request(sock, msg):
    sock.send(msg.encode('utf-8'))
    return parse_response(sock)

def parse_response(sock):
    pair = sock.recvfrom(1024)
    #print('recv from', pair[1])
    resp = pair[0].decode('utf-8')
    params = resp.split(' ')
    if params[0] == 'OK':
        return params[1:]
    print(params[0])


def udp_hole_punch_A(sock, args):
    resp = request(sock, 'REGISTER %s\n' % args.key)
    print(resp)

    params = request(sock, 'QUERY \n')
    dict = json.loads(params[0])
    for k in dict:
        print(dict[k])

    print('Enter to start recv ready')

    resp = parse_response(sock)
    print('recv resp:', resp)

    addr = resp[1].split(':')
    msg = 'a quick brown fox jumps over the lazy dog'
    sock.sendto(msg.encode('utf-8'), (addr[0], int(addr[1])))
    print('sent message to', addr)

    resp = parse_response(sock)
    print('recv resp:', resp)


def udp_hole_punch_B(sock, args):
    resp = request(sock, 'REGISTER %s\n' % args.key)
    print(resp)

    params = request(sock, 'QUERY \n')
    dict = json.loads(params[0])

    target = None
    for k in dict:
        if k != args.key:
            target = dict[k]
            break

    if target == None:
        print('no target found')
        return

    print('target is', target)

    # make a NAT hole [self -> target]
    msg = 'OK READY %s\n' % args.key
    sock.sendto(msg.encode('utf-8'), (target['ip'], int(target['port'])))
    print('sent dummy packet to', target['ip'], target['port'])

    print('send READY to proxy server')
    resp = request(sock, 'READY %s\n' % target['key'])
    print('recv ready resp:', resp)

    print('start recv peer message:')
    resp = parse_response(sock)
    print(resp)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", type=str, default='A')
    parser.add_argument("--host", type=str, default='3bodygame.com')
    parser.add_argument("--port", type=int, default=8082)
    parser.add_argument("--key", type=str)
    args = parser.parse_args()

    sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, proto=socket.IPPROTO_UDP)
    sock.connect((args.host, args.port))

    if args.mode == 'A':
        udp_hole_punch_A(sock, args)
    else:
        udp_hole_punch_B(sock, args)

    sock.close()


if __name__ == '__main__':
    main()