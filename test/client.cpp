// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the BSD 3-Clause License.
// See accompanying files LICENSE.

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/socket.h>
#include <netdb.h>
#include <functional>
#include <arpa/inet.h>


// create a TCP connection
int create_connection(const char* host, const char* port);

// send request to server
static int send_request(int fd, const char* command, int cmdlen, char* buffer, int buflen);

// bind a UDP socket
static int bind_udp(const sockaddr_in* addr);


// compile with: g++ -g -o client client.cpp
int main(int argc, char* argv[])
{
    const char* host = "3bodygame.com";
    const char* port = "8082";
    const char* key = "helloworld";
    switch (argc)
    {
        case 4:
            key = argv[3];
        case 3:
            port = argv[2];
        case 2:
            host = argv[1];
            break;
        default:
            break;
    }

    int fd = create_connection(host, port);
    if (fd < 0)
    {
        return 1;
    }

    char buffer[1024];
    char command[80];
    int n = snprintf(command, 80, "REGISTER %s\n", key);
    int r = send_request(fd, command, n, buffer, 1024);
    if (r < 0)
    {
        close(fd);
        return 1;
    }
    fprintf(stdout, "%s\n", buffer+4);

    memset(command, 0, 80);
    n = snprintf(command, 80, "QUERY \n");
    r = send_request(fd, command, n, buffer, 1024);
    if (r < 0)
    {
        close(fd);
        return 1;
    }
    fprintf(stdout, "%s\n", buffer+4);

    sockaddr_in addr = {};
    socklen_t len = sizeof(addr);
    r = getsockname(fd, (struct sockaddr*)&addr, &len);
    if (r < 0)
    {
        close(fd);
        return 1;
    }
    fprintf(stdout, "local port: %d\n", ntohs(addr.sin_port));

    addr.sin_addr.s_addr = INADDR_ANY;
    int ufd = bind_udp(&addr);
    if (ufd < 0)
    {
        close(fd);
        return 1;
    }

    fprintf(stdout, "wait for UDP packet...\n");
    sockaddr_in remote;
    for (int i = 0; i < 3; i++)
    {
        r = recvfrom(ufd, buffer, 1024, 0, (sockaddr*)&addr, &len);
        if (r < 0)
        {
            fprintf(stderr, "recvfrom: %s\n",strerror(errno));
            close(ufd);
            close(fd);
            return 1;
        }

        buffer[r] = '\0';
        fprintf(stdout, "%s\n", buffer);

        r = sendto(fd, buffer, r, 0, (sockaddr*)&addr, len);
        if (r < 0)
        {
            fprintf(stderr, "sendto: %s\n",strerror(errno));
            close(ufd);
            close(fd);
            return 1;
        }
    }

    return 0;
}


// create a TCP connection
int create_connection(const char* host, const char* port)
{
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo* aiList = NULL;
    int err = getaddrinfo(host, port, &hints, &aiList);
    if (err != 0)
    {
        fprintf(stderr, "%s\n", gai_strerror(err));
        return -1;
    }
    struct addrinfo* pai = NULL;
    int fd = -1;
    for (pai = aiList; pai != NULL; pai = aiList->ai_next)
    {
        fd = socket(pai->ai_family, pai->ai_socktype, pai->ai_protocol);
        if (fd < 0)
        {
            fprintf(stderr, "socket: %s\n", strerror(errno));
            continue;
        }
        int value = 1;
        int err = setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &value, sizeof(value));
        if (err < 0)
        {
            fprintf(stderr, "setsockopt: %s\n",strerror(errno));
            close(fd);
            fd = -1;
            continue;
        }
        err = connect(fd, pai->ai_addr, pai->ai_addrlen);
        if (err < 0)
        {
            fprintf(stderr, "setsockopt: %s\n",strerror(errno));
            close(fd);
            fd = -1;
            continue;
        }
        break;
    }
    freeaddrinfo(aiList);
    return fd;
}

// bind a UDP socket
int bind_udp(const sockaddr_in* addr)
{
    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (fd < 0)
    {
        return -1;
    }
    int value = 1;
    int err = setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &value, sizeof(value));
    if (err < 0)
    {
        fprintf(stderr, "setsockopt: %s\n",strerror(errno));
        close(fd);
        return -1;
    }
    err = bind(fd, (const sockaddr*)addr, sizeof(*addr));
    if (err < 0)
    {
        fprintf(stderr, "bind: %s\n", strerror(errno));
        close(fd);
        return -1;
    }
    return fd;
}

// send request to server
int send_request(int fd, const char* command, int cmdlen, char* buffer, int buflen)
{
    int n = send(fd, command, cmdlen, 0);
    if (n < 0)
    {
        fprintf(stderr, "send: %s\n",strerror(errno));
        return -1;
    }

    n = recv(fd, buffer, buflen, 0);
    if (n <= 0)
    {
        fprintf(stderr, "recv: %s\n",strerror(errno));
        return -1;
    }
    buffer[n] = '\0'; // null-terminated
    return 0;
}


