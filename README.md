# nat
a simple UDP hole punching under NAT network

```
go get -v github.com/ichenq/nat
go install github.com/ichenq/nat/apps/proxy
```

## 简介

proxy提供UDP服务

* `DISCOVER` 简单的检查NAT类型是Port Restricted还是Address Restricted
* `REGISTER` 注册一个key，value为自身NAT外部地址
* `QUERY` 根据key查询一个value
* `READY` 通知另一个peer就绪

## 使用方式

在服务器上运行proxy

在不同的NAT环境中运行test/udp_punch.py
